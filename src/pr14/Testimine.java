package pr14;

public class Testimine {

	public static void main(String[] args) {

		Punkt minuPunkt = new Punkt(100, 200);
		Punkt sinuPunkt = new Punkt(150, 300);
		System.out.println(minuPunkt);
		System.out.println(sinuPunkt);
		
		Joon minuJoon = new Joon(minuPunkt, sinuPunkt);
		System.out.println(minuJoon);
		System.out.println("Joonepikkus on " + minuJoon.pikkus());
		
		Ring minuRing = new Ring(minuPunkt, minuJoon.pikkus());
		System.out.println("Ringi ümbermõõt on " + minuRing.ymberm66t());
		System.out.println("Ringi pindala on " + minuRing.pindala());
		
		Silinder minuSilinder = new Silinder(minuRing, 100);
		System.out.println(minuSilinder);
	}

}
