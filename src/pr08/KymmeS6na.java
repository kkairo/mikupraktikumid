package pr08;

public class KymmeS6na {

	public static void main(String[] args) {

		// Defineerime massiivi
		String[] s6nad = new String[10];

		// for-tsykli abil laseme kasutajal sisestada t2pselt 10 s6na
		for (int i = 0; i < s6nad.length; i++) {
			System.out.println("Sisesta sõna: ");
			s6nad[i] = TextIO.getlnString();
		}

		// for-tsykkel, mis prindib s6na pikkuse ja s6na ise
		for (int i = 0; i < s6nad.length; i++) {
			int s6naPikkus = s6nad[i].length();
			System.out.println(s6naPikkus + " " + s6nad[i]);
		}

		// V6IB KA NII
		// for (String s6na : s6nad) {
		// int s6naPikkus = s6na.length();
		// System.out.println(s6naPikkus + " " + s6na);
		// }
	}

}
