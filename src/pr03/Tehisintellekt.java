package pr03;

import lib.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {

		// Muutujate defineerimine
		int vanus1;
		int vanus2;
		int vanem;
		int noorem;

		// Vanuste sisestamine, kus while ts�kkel kontrollib, kas vanuste
		// v��rtused on reaalsed
		while (true) {

			System.out.print("Siseta esimene vanus: ");
			vanus1 = TextIO.getInt();
			System.out.print("Siseta teine vanus: ");
			vanus2 = TextIO.getInt();

			if (vanus1 > 0 && vanus2 > 0) {
				break;
			} else {
				System.out.println("Sisesta positiivsed v��rtused!");
			}
		}

		// Vanuste �mberdefineerimine, et vanused ei s�ltu sisestamise
		// j�rjekorrast
		if (vanus1 > vanus2) {
			vanem = vanus1;
			noorem = vanus2;
		} else {
			vanem = vanus2;
			noorem = vanus1;
		}

		// kui oled vanem, kui 5 kuni 10 aastat
		if (vanem > noorem + 5 && vanem <= noorem + 10) {
			System.out.println("Otsi endale vanem s�ber");
		}

		// kui oled vanem, kui 10 aastat
		else if (vanem > noorem + 10) {
			System.out.println("Sa arvatavasti sured varem, kui su s�brad");
		}

		// kui ole noorem v�i mitte vanem, kui 5 aastat
		else {
			System.out.println("K�ik on fain.");

		}
	}
}
